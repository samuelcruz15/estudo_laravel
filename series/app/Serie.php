<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Serie extends Model
{
    //protected $table= 'series'
    //Laravel por padrao utilizar o nome da tabela pegando a classe, depois minusculo, depois plura, ou seja = series

    //Parar criar o model e sua migratio: php artisan make:model Serie -m

    public $timestamps = false;
    protected $fillable = ['nome'];

    //Precisa de um metodo para fazer a relação de serie com temporadas
    public function temporadas()
    {
        return $this->hasMany(Temporada::class);
    }
}
