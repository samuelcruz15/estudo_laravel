<?php

namespace App\Services;

use App\{Episodio, Serie, Temporada};
use Illuminate\Support\Facades\DB;

class RemovedorDeSerie
{
    //Metodo retorna uma string
    public function removerSerie(int $serieId): string
    {
        $nomeSerie = '';
        /* 1-transaction executa todas as ações no banco de uma única vez. Se uma der erro, nao se perde os dados 
        (nesse caso de delete)
        2-use referencia os valores que vai usar dentro da função
        3-Colocamos & no $nomeSerie pois estamos pegando e referenciando o valor real, ou seja, oq é modificado
         em $nomeSerie, realmente é guardado, e nao apenas dentro da função como $serieId
         */
        DB::transaction(function () use ($serieId, &$nomeSerie) {
            $serie = Serie::find($serieId);
            $nomeSerie = $serie->nome;
            /* Definitivo
            //each é como se fosse um laco de repetição
            $serie->temporadas->each(function (Temporada $temporada) {
                $temporada->episodios()->each(function (Episodio $episodio) {
                    $episodio->delete();
                });
                $temporada->delete();
            });*/
            $this->removerTemporadas($serie);
            $serie->delete();
        });

        return $nomeSerie;
    }

    private function removerTemporadas(Serie $serie): void
    {
        $serie->temporadas->each(function (Temporada $temporada) {
            $this->removerEpisodios($temporada);
            $temporada->delete();
        });
    }

    private function removerEpisodios(Temporada $temporada): void
    {
        $temporada->episodios()->each(function (Episodio $episodio) {
            $episodio->delete();
        });
    }
}
