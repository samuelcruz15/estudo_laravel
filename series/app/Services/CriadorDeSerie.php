<?php

namespace App\Services;

use App\Serie;
use Illuminate\Support\Facades\DB;

class CriadordeSerie
{
    //Metodo retorna objeto criado (Serie)
    public function criarSerie(String $nome, int $qtdTemporadas, int $epPorTemporada): Serie
    {
        //Comando para começar a transação, sem passar os parametrods que vai ser usada nela
        DB::beginTransaction();
        $serie = Serie::create(['nome' => $nome]);
        $this->criaTemporadas($qtdTemporadas, $epPorTemporada, $serie);
        DB::commit();
        //Comando para executar a transação

        return $serie;
    }

    private function criaTemporadas(int $qtdTemporadas, int $epPorTemporada, Serie $serie)
    {
        for ($i = 1; $i <= $qtdTemporadas; $i++) {
            //Na serie que foi criada, vamos criar as temporadas(Eloquente ja relaciona pra gente)
            $temporada = $serie->temporadas()->create(['numero' => $i]);

            $this->criarEpisodios($epPorTemporada, $temporada);
        }
    }

    private function criarEpisodios(int $epPorTemporada, \Illuminate\Database\Eloquent\Model $temporada): void
    {
        for ($x = 1; $x <= $epPorTemporada; $x++) {
            $temporada->episodios()->create(['numero' => $x]);
        }
    }
}
