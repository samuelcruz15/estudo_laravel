<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Autenticador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /*Verificação para liberar as rotas que não precisam de Autenticação
        Nesse caso estamos colocando autenticação em toda rota web (app/Requests/Kernel.php - > $middlewareGroups) e 
        Colocamos a classe Auntenticado lá
        Mas não usaremos aqui pois ainda existe rotas na aplicação que não precisam de autenticação
        */
        if (!$request->is('entrar', 'registrar') && !Auth::check()) {
            return redirect('/entrar');
        }
        return $next($request);
    }
}
