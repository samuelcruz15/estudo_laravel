<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SeriesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|min:2',
            'qtd_temporadas' => 'required|numeric|min:1',
            'ep_por_temporada' => 'required|numeric|min:1'
        ];
    }
    public function messages()
    {
        return [
            //especifico 'nome.required' => 'Nome obrigatório',
            //geral
            'required' => ':attribute obrigatório',
            'nome.min' => 'Minimo de 2 caracteres',
            'qtd_temporadas.min' => 'Minimo de 1 Temporada',
            'ep_por_temporada.min' => 'Minimo de 1 Episódio',
        ];
    }
}
