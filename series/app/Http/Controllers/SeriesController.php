<?php

namespace App\Http\Controllers;

use App\Episodio;
use App\Http\Requests\SeriesFormRequest;
use App\Serie;
use App\Services\CriadordeSerie;
use App\Services\RemovedorDeSerie;
use App\Temporada;
use finfo;
use Illuminate\Http\Request;

class SeriesController extends Controller
{
    /* //METODO 2 -Caso precisse proteger todo o controller
    public function __construct()
    {
        $this->middleware('auth');
    }*/
    public function index(Request $request)
    {
        $series = Serie::query()->orderBy('nome')->get();
        //$series = Serie::all();

        $mensagem = $request->session()->get('mensagem');
        //Caso queira remover  $request->session()->remove('mensagem'); (se nao for mensagem flash(que se auto destrui))
        return view('series.index', compact('series', 'mensagem'));
    }

    public function create()
    {
        return view('series.create');
    }

    public function store(SeriesFormRequest $request, CriadordeSerie $criadordeSerie)
    {

        //$nome = $request->nome;
        /* 1° forma 
        $serie = new Serie();
        $serie->nome = $nome;
        $serie->save();*/

        /*2° Forma
        $serie = Serie::create([
            'nome' => $nome
        ]); */

        //Maneira para criar tudo oq vem do formulario (name tem que ser igual ao do banco) 
        //$serie = Serie::create($request->all());

        /* Criação definitiva (foi jogada para App\Services para melhor pratica de codigo)
        $serie = Serie::create(['nome' => $request->nome]);
        $qtdTemporadas = $request->qtd_temporadas;
        for ($i = 1; $i <= $qtdTemporadas; $i++) {
            //Na serie que foi criada, vamos criar as temporadas(Eloquente ja relaciona pra gente)

            $temporada = $serie->temporadas()->create(['numero' => $i]);

            for ($x = 1; $x <= $request->ep_por_temporada; $x++) {
                $temporada->episodios()->create(['numero' => $x]);
            }
        } 
        */
        $serie = $criadordeSerie->criarSerie($request->nome, $request->qtd_temporadas, $request->ep_por_temporada);


        //echo "Série com id {$serie->id} criada: {$serie->nome}";
        //put = durar mais de um sessao
        //flas = apenas um requisição
        $request->session()->flash(
            'mensagem',
            "Série {$serie->nome} e suas temporadas e episódios criados com sucesso"
        );
        return redirect()->route('listar_series');;
    }

    public function destroy(Request $request, RemovedorDeSerie $removedorDeSerie)
    {
        /* Delete definitido ()
        $serie = Serie::find($request->id);
        $nomeSerie = $serie->nome;
        //each é como se fosse um laco de repetição
        $serie->temporadas->each(function (Temporada $temporada) {
            $temporada->episodios()->each(function (Episodio $episodio) {
                $episodio->delete();
            });
            $temporada->delete();
        });

        $serie->delete();
        */
        $nomeSerie = $removedorDeSerie->removerSerie($request->id);
        $request->session()->flash(
            'mensagem',
            "Série $nomeSerie removida com sucesso"
        );
        return redirect()->route('listar_series');
    }

    public function editaNome(int $serieId, Request $request)
    {
        $novoNome = $request->nome;
        $serie = Serie::find($serieId);
        $serie->nome = $novoNome;
        $serie->save();
    }
}
