<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegistroController extends Controller
{
    public function create()
    {
        return view('registro.create');
    }

    public function store(Request $request)
    {
        //Pegar todos os dados do Request exceto o _token(@csrf)
        $dadosUsuario = $request->except('_token');
        //Usando a própia criptografia do Laravel
        $dadosUsuario['password'] = Hash::make($dadosUsuario['password']);
        $user = User::create($dadosUsuario);

        Auth::login($user);

        return redirect()->route('listar_series');
    }
}
