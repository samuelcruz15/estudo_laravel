<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EntrarController extends Controller
{
    public function index()
    {
        return view('entrar.index');
    }

    public function entrar(Request $request)
    {
        //metodo only() array associativo com o e-mail e a senha do usuário que mandou a tentativa de login
        //Pegamos o email e password do usuario e acionamos o metodo do laravel Auth::attempt() para "tentar logar" com os dados

        if (!Auth::attempt($request->only(['email', 'password']))) {
            return redirect()
                ->back()
                ->withErrors('Usuário e/ou senha incorretos');
        }

        return redirect()->route('listar_series');
    }
}
