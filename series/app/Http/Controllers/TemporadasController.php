<?php

namespace App\Http\Controllers;

use App\Serie;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class TemporadasController extends Controller
{
    public function index(int $serieId)
    {
        $serie = Serie::find($serieId);
        //$temporadas =  Temporada::query()->where('serie_id', $serieId)->orderBy('numero')->get();
        $temporadas = Serie::find($serieId)->temporadas;

        return view('temporadas.index', compact('serie', 'temporadas'));
    }
}
