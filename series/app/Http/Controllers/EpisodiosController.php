<?php

namespace App\Http\Controllers;

use App\Episodio;
use App\Temporada;
use Illuminate\Http\Request;

class EpisodiosController extends Controller
{
    public function index(Temporada $temporada, Request $request)
    {
        //Atribuindo a Model Temporada, automaticamente o Laravel ja faz um Find e busca qual temporada passamos como parametro
        //$episodios = $temporada->episodios;

        // return view('episodios.index', compact('episodios', 'temporada'));

        return view('episodios.index', [
            'episodios' => $temporada->episodios,
            'temporada' => $temporada,
            'mensagem' => $request->session()->get('mensagem')
        ]);
    }

    public function assistir(Temporada $temporada, Request $request)
    {
        //Vamos marcar todos os episodios como falso
        //A não ser os que foram enviados como true

        $episodiosAssistidos = $request->episodios;
        //O método recebe uma função, que será executada uma vez para cada item na coleção, 
        //recebendo este item como parâmetro.
        $temporada->episodios->each(function (Episodio $episodio) use ($episodiosAssistidos) {
            //se estiver no in_array vai se true, se não false
            $episodio->assistido = in_array($episodio->id, $episodiosAssistidos);
            // Update Padrão
            //$episodio->save();
        });
        //Executa tudo de modificação da temporada e Episodio ao mesmo tempo
        $temporada->push();

        $request->session()->flash('mensagem', 'Ação realizada com Sucesso');
        return redirect()->back();
    }
}
