@extends('layout')

@section('cabecalho')
    Temporadas de <b>{{$serie->nome}}</b>
@endsection

@section('conteudo')
    


<ul class="list-goup">
    @foreach ($temporadas as $temporada )
        <li class="list-group-item d-flex justify-content-between align-items-center">
          <a href="/temporadas/{{$temporada->id}}/episodios" class="">
                Temporada {{$temporada->numero}}
            </a>
        <span class="badge badge-secondary">
            <!--Através das Collections ja conseguimos pegar a relação da quantidade de episodios por temporada -->
            {{ $temporada->getEpisodiosAssistidos()->count() }}/{{$temporada->Episodios()->count()}}
        </span>
    </li>
    @endforeach
</ul>
@endsection