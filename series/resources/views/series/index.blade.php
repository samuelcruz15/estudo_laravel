@extends('layout')

@section('cabecalho')
    Séries
@endsection

@section('conteudo')

@if(!empty($mensagem))
<div class="alert alert-success">
    {{$mensagem}}
</div>
@endif

@auth
<a href="{{route('form_criar_serie')}}" class="btn btn-dark mb-2">Adicionar</a>
@endauth

<ul class="list-group">
    @foreach ($series as $serie)
      <li class="list-group-item d-flex justify-content-between align-items-center">
        
        <span id="nome-serie-{{ $serie->id }}">{{ $serie->nome }}</span>
@auth
            <div class="input-group w-50" hidden id="input-nome-serie-{{ $serie->id }}">
                <input type="text" class="form-control" value="{{ $serie->nome }}">
                <div class="input-group-append">
                    <button class="btn btn-primary" onclick="editarSerie({{ $serie->id }})">
                        <i class="fas fa-check"></i>
                    </button>
                    @csrf
                </div>
            </div>
@endauth
            <span class="d-flex">
 @auth
                <button class="btn btn-secondary btn-sm mr-1" onclick="toggleInput({{$serie->id}})">
                    <i class="fas fa-edit"></i>
                    </button> 
@endauth
            <a href="/series/{{$serie->id}}/temporadas" class="btn btn-info btn-sm mr-1">
                <i class="fas fa-external-link-alt"></i>
            </a>
 @auth
            <form method="Post" action="/series/{{$serie->id}}"
                onsubmit="return confirm('Tem certeza que deseja remover {{addslashes($serie->nome)}}')">
                @csrf
                @method('DELETE')
           <button class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
            </form>
 @endauth
        </span>
        </li>
    @endforeach
</ul>

<script>
    function toggleInput(serieId){

   const nomeSerieEl = document.getElementById(`nome-serie-${serieId}`);
   const inputSerieEl = document.getElementById(`input-nome-serie-${serieId}`);
    if (nomeSerieEl.hasAttribute('hidden')) {
        nomeSerieEl.removeAttribute('hidden');
        inputSerieEl.hidden = true;
    } else {
        inputSerieEl.removeAttribute('hidden');
        nomeSerieEl.hidden = true;
    }
    }

    function editarSerie(serieId) {
        //Nomeando um variavel como um formulario js
let formData= new FormData();

        //Pegando um input que é filho direto do id (id esta na div principal)
    const nome = document
        .querySelector(`#input-nome-serie-${serieId} > input`)
        .value;

        //pegando token laravel formulario
        const token=document.querySelector('input[name="_token"]').value;

        //adicionando valores ao formulario
        formData.append('nome',nome);
        formData.append('_token',token);

//Enviando requisição (parecido ajax)
        const url =`/series/${serieId}/editaNome`;
        fetch(url,{
body: formData,
method: 'Post'
        }).then(()=>{
            toggleInput(serieId);
            const nomeSerieEl = document.getElementById(`nome-serie-${serieId}`).textContent=nome;
        });
}

</script>
@endsection
      
 