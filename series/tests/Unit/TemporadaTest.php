<?php
//php artisan make:test TemporadaTeste --unit (comando de criação)
//    vendor\bin\phpunit Executa todos os testes

namespace Tests\Unit;

use App\Episodio;
use App\Temporada;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TemporadaTeste extends TestCase
{
    use RefreshDatabase;

    private $temporada;
    //Função setUp executa esse código antes de cada Teste
    protected function setUp(): void
    { //Criando episodio em uma temporada qualquer
        parent::setUp();
        $temporada = new Temporada();
        $episodio1 = new Episodio();
        $episodio1->assistido = true;
        $episodio2 = new Episodio();
        $episodio2->assistido = false;
        $episodio3 = new Episodio();
        $episodio3->assistido = true;
        $temporada->episodios->add($episodio1);
        $temporada->episodios->add($episodio2);
        $temporada->episodios->add($episodio3);

        $this->temporada = $temporada;
    }

    public function testBuscaPorEpisodiosAssistidos()
    { //Verificar quantos episoduis foram assistidos
        $episodiosAssistidos = $this->temporada->getEpisodiosAssistidos();
        $this->assertCount(2, $episodiosAssistidos);
        foreach ($episodiosAssistidos as $episodio) {
            $this->assertTrue($episodio->assistido);
        }
    }

    public function testBuscaTodosOsEpisodios()
    { //Teste para saber se existem 3 temporadas 
        $episodios = $this->temporada->episodios;
        $this->assertCount(3, $episodios);
    }
}
