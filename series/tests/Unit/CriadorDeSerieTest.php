<?php

namespace Tests\Unit;

use App\Serie;
use App\Services\CriadordeSerie;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CriadorDeSerieTest extends TestCase
{

    use RefreshDatabase;

    public function testCriarSerie()
    {
        $criadorDeSerie = new CriadordeSerie();
        $nomeSerie = 'Nome de teste';
        $serieCriada = $criadorDeSerie->criarSerie($nomeSerie, 1, 1);

        //Verificando se a Instancia foi criada
        $this->assertInstanceOf(Serie::class, $serieCriada);
        //Verificando se existe o dado criado  no banco de Dados
        $this->assertDatabaseHas('series', ['nome' => $nomeSerie]);
        $this->assertDatabaseHas('temporadas', ['serie_id' => $serieCriada->id, 'numero' => 1]);
        $this->assertDatabaseHas('episodios', ['numero' => 1]);
    }
}
