<?php

namespace Tests\Feature;

use App\Services\CriadordeSerie;
use App\Services\RemovedorDeSerie;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RemovedorSerieTest extends TestCase
{
    use RefreshDatabase;
    /** @var Serie */
    private $serie;
    //Primeiro vamos criar uma serie para remover
    protected function setUp(): void
    {
        parent::setUp();
        $criadorDeSerie = new CriadordeSerie();
        $this->serie = $criadorDeSerie->criarSerie(
            'Nome da série',
            1,
            1
        );
    }

    public function testRemoverUmaSerie()
    {
        //Verificando se existe o registro criado
        $this->assertDatabaseHas('series', ['id' => $this->serie->id]);
        $removedorDeSerie = new RemovedorDeSerie();
        $nomeSerie = $removedorDeSerie->removerSerie($this->serie->id);
        //O retorno é uma string
        $this->assertIsString($nomeSerie);
        //Verificando se é esse mesmo a serie removida
        $this->assertEquals('Nome da série', $this->serie->nome);
        //Verificando se não possui o registro no banco (ou seja, foi excluido)
        $this->assertDatabaseMissing('series', ['id' => $this->serie->id]);
    }
}
